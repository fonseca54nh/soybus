document.addEventListener('deviceready', onDeviceReady, false);

let map
let marker
let ws
let markers = []

let stopIcon = L.icon({
        iconUrl: 'img/stop.png',
        iconSize: [50, 50]
    });

let busIcon = L.icon({
        iconUrl: 'img/bus2.png',
        iconSize: [50, 50]
    });

function create( pos, name )
{
    for( var i = 0; i < pos[0].length; ++i )
    {
        marker = L.marker([pos[0][i], pos[1][i]]).bindTooltip( name[i] )
        marker.setIcon(busIcon);
        marker.addTo(map);
        markers.push( marker )
    }
}

function setBusLocation( pos, x )
{
        
        lat = L.latLng( pos[0], pos[1] )
        markers[ x ].setLatLng( lat )
}

function setStops( pos, name )
{
    for( var i = 0; i < pos[0].length; ++i )
    {
        marker = L.marker([pos[0][i], pos[1][i]]).bindTooltip( name[i] )
        marker.setIcon(stopIcon);
        marker.addTo(map);
    }
}

getLoc = { type: 'getLoc' }

function onDeviceReady() {

    map = L.map('location-map').setView([	-23.55647979252711, -46.617891363861965], 17);
    mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';
    L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; ' + mapLink,
      maxZoom: 20,
    }).addTo(map);

    ws = new WebSocket('ws://192.168.0.2:10000')

    ws.onopen = () => { setInterval( function() { ws.send( JSON.stringify( getLoc ) ) }, 1000 ) }
    

    ws.onmessage = ( { data } ) => 
    {
        data = JSON.parse( data ) 
        console.log( data )
        type = data.type

        if( type === 'busStp' ) { setStops( data.vec, data.stpNames ), create( data.start, data.name ) }
        if( type === 'busLoc' ) 
        { 
            console.log( data )
            for( i = 0; i < data.vec.length; ++i ) { setBusLocation( data.vec[i], i ) }
        }
    }
}
